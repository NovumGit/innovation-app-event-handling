export interface BasePromiseType extends PromiseLike<any> {
    onFulfilled(callback: (arg: any) => any): void;
    onRejected(callback: (arg: any) => any): void;
}
