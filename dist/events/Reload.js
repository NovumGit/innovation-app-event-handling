var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "../AbstractEvent"], function (require, exports, AbstractEvent_1) {
    "use strict";
    exports.__esModule = true;
    var Reload = (function (_super) {
        __extends(Reload, _super);
        function Reload() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Reload.prototype.trigger = function () {
            return new Promise(function () {
                window.location = window.location;
            });
        };
        return Reload;
    }(AbstractEvent_1.AbstractEvent));
    exports.Reload = Reload;
});
//# sourceMappingURL=Reload.js.map