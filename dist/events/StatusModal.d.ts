import { IEvent } from "../IEvent";
import { AbstractEvent } from "../AbstractEvent";
import { StatusState } from "../types/StatusState";
import { BasePromiseType } from "./base/BasePromiseType";
import { StatusModalProps } from "../types/StatusModal/StatusModalProps";
export declare class StatusModal<P extends StatusModalProps, S extends StatusState, PT extends BasePromiseType> extends AbstractEvent<P, S, PT> implements IEvent<P, S, PT> {
    propType: P;
    stateType: S;
    promiseType: PT;
    trigger(props: P, state: S): Promise<PT>;
    private getTemplate;
    private show;
}
