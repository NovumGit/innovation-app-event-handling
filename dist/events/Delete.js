var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "../AbstractEvent", "../HttpCall", "../types/Http/HttpMethod"], function (require, exports, AbstractEvent_1, HttpCall_1, HttpMethod_1) {
    "use strict";
    exports.__esModule = true;
    var Delete = (function (_super) {
        __extends(Delete, _super);
        function Delete() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Delete.prototype.trigger = function (props, state) {
            var _this = this;
            return new Promise(function (resolve, reject) {
                var sUrl = props.datasource.url + props.endpoint;
                var rejectHandler = _this.getRejectHandler(reject, "Whoops", "Sorry we where unable to delete that item\n {textStatus}");
                HttpCall_1.HttpCall(sUrl, HttpMethod_1.HttpMethod.DELETE, '', resolve, rejectHandler);
            });
        };
        return Delete;
    }(AbstractEvent_1.AbstractEvent));
    exports.Delete = Delete;
});
//# sourceMappingURL=Delete.js.map