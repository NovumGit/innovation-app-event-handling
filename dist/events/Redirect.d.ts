import { IEvent } from "../IEvent";
import { AbstractEvent } from "../AbstractEvent";
import { BasePromiseType } from "./base/BasePromiseType";
import { RedirectState } from "../types/Redirect/RedirectState";
import { RedirectProps } from "../types/Redirect/RedirectProps";
export declare class Redirect<P extends RedirectProps, S extends RedirectState, PT extends BasePromiseType> extends AbstractEvent<P, S, PT> implements IEvent<P, S, PT> {
    propType: P;
    stateType: S;
    promiseType: PT;
    trigger(props: P): Promise<PT>;
}
