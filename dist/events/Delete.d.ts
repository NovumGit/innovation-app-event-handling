import { AbstractEvent } from "../AbstractEvent";
import { IEvent } from "../IEvent";
import { BasePromiseType } from "./base/BasePromiseType";
import { DeleteProps } from "../types/Delete/DeleteProps";
import { DeleteState } from "../types/Delete/DeleteState";
export declare class Delete<P extends DeleteProps, S extends DeleteState, PT extends BasePromiseType> extends AbstractEvent<DeleteProps, DeleteState, BasePromiseType> implements IEvent<DeleteProps, DeleteState, BasePromiseType> {
    stateType: S;
    propType: P;
    promiseType: PT;
    trigger(props: P, state: S): Promise<PT>;
}
