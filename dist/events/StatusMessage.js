var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "../types/StatusMessage/StatusMessage", "../AbstractEvent"], function (require, exports, StatusMessage_1, AbstractEvent_1) {
    "use strict";
    exports.__esModule = true;
    var StatusMessage = (function (_super) {
        __extends(StatusMessage, _super);
        function StatusMessage() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        StatusMessage.prototype.getPropType = function () {
            return this.propType;
        };
        StatusMessage.prototype.getStateType = function () {
            return this.stateType;
        };
        StatusMessage.prototype.trigger = function (props, state) {
            return new Promise(function (resolve) {
                if (state instanceof StatusMessage_1.StatusValidationState) {
                    state.errors.forEach(function (e) {
                        state.message = "- " + e + "\n";
                    });
                }
                if (state.message) {
                    StatusMessage.show(state.StatusMessageColor, state.message, state.title, state.elementId);
                }
                resolve("Showing status message");
            });
        };
        StatusMessage.getTemplate = function (sType, sLabel, sMessage, sElementId) {
            var sLabelColor = sType === 'warning' ? 'black' : 'white';
            var sTemplate = '';
            if ($('#' + sElementId).length <= 0) {
                sTemplate += '       <div class="bs-component" id="' + sElementId + '">';
                sTemplate += '           <div class="alert alert-' + sType + ' dark alert-dismissable" style="color:' + sLabelColor + ';">';
                sTemplate += '               <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
                sTemplate += '               <i class="fa fa-info pr10" />';
                sTemplate += '               <strong>' + sLabel + ':</strong> ' + sMessage;
                sTemplate += '           </div>';
                sTemplate += '       </div>';
            }
            else {
                console.log("Already showing message with id " + sElementId + ", skipping for now.");
            }
            return sTemplate;
        };
        StatusMessage.show = function (sType, sMessage, sTitle, sElementId) {
            var sTemplate = StatusMessage.getTemplate(sType, sTitle, sMessage, sElementId);
            if (sTemplate) {
                $('#status_message_container').html(sTemplate);
            }
        };
        return StatusMessage;
    }(AbstractEvent_1.AbstractEvent));
    exports.StatusMessage = StatusMessage;
});
//# sourceMappingURL=StatusMessage.js.map