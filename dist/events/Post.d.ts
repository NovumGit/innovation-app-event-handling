import * as t from "../types/overheid";
import { AbstractEvent } from "../AbstractEvent";
import { IEvent } from "../IEvent";
import { BaseProps } from "./base/BaseProps";
import { BaseState } from "./base/BaseState";
import { BasePromiseType } from "./base/BasePromiseType";
export interface PostProps extends BaseProps {
    datasource: t.datasource;
    defaults: {};
    endpoint: string;
}
export interface PostState extends BaseState {
    context: {
        form: {
            id: string;
        };
    };
}
export declare class Post<P extends PostProps, S extends PostState, PT extends BasePromiseType> extends AbstractEvent<BaseProps, BaseState, BasePromiseType> implements IEvent<BaseProps, BaseState, BasePromiseType> {
    propType: BaseProps;
    stateType: BaseState;
    promiseType: BasePromiseType;
    trigger(props: P, state: S): Promise<PT>;
}
