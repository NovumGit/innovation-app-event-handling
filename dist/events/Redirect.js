var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "../AbstractEvent"], function (require, exports, AbstractEvent_1) {
    "use strict";
    exports.__esModule = true;
    var Redirect = (function (_super) {
        __extends(Redirect, _super);
        function Redirect() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Redirect.prototype.trigger = function (props) {
            return new Promise(function (resolve) {
                window.location.href = props.location;
                resolve("Redirecting the user to: " + props.location);
            });
        };
        return Redirect;
    }(AbstractEvent_1.AbstractEvent));
    exports.Redirect = Redirect;
});
//# sourceMappingURL=Redirect.js.map