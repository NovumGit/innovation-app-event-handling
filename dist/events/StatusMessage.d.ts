import { IEvent } from "../IEvent";
import { AbstractEvent } from "../AbstractEvent";
import { BaseProps } from "./base/BaseProps";
import { StatusState } from "../types/StatusState";
import { BasePromiseType } from "./base/BasePromiseType";
export interface StatusMessageProps extends BaseProps {
}
export declare class StatusMessage<P extends StatusMessageProps, S extends StatusState, PT extends BasePromiseType> extends AbstractEvent<P, S, PT> implements IEvent<P, S, PT> {
    promiseType: PT;
    propType: P;
    stateType: S;
    getPropType(): P;
    getStateType(): S;
    trigger(props: P, state: StatusState): Promise<PT>;
    private static getTemplate;
    private static show;
}
