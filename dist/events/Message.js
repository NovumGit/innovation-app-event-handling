var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "../Util", "../AbstractEvent", "../types/Http/HttpMethod", "../HttpCall"], function (require, exports, Util_1, AbstractEvent_1, HttpMethod_1, HttpCall_1) {
    "use strict";
    exports.__esModule = true;
    var Message = (function (_super) {
        __extends(Message, _super);
        function Message() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Message.prototype.trigger = function (props, state) {
            var _this = this;
            return new Promise(function (resolve, reject) {
                var sUrl = props.datasource.url + props.endpoint;
                var data = Util_1.Util.parseForm(state.context.form.id);
                if (props.defaults) {
                    data = Object.assign(props.defaults, data);
                }
                var rejectHandler = _this.getRejectHandler(reject, "Whoops", "Het wijzigen is mislukt");
                HttpCall_1.HttpCall(sUrl, HttpMethod_1.HttpMethod.POST, JSON.stringify(data), resolve, rejectHandler);
            });
        };
        return Message;
    }(AbstractEvent_1.AbstractEvent));
    exports.Message = Message;
});
//# sourceMappingURL=Message.js.map