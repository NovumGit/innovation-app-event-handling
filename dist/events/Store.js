var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "../Util", "./Post", "./Patch", "../AbstractEvent"], function (require, exports, Util_1, Post_1, Patch_1, AbstractEvent_1) {
    "use strict";
    exports.__esModule = true;
    var Store = (function (_super) {
        __extends(Store, _super);
        function Store() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Store.prototype.trigger = function (props, state) {
            return new Promise(function (resolve, reject) {
                var formData = Util_1.Util.parseForm(state.context.form.id);
                console.log('IForm data');
                console.log(JSON.stringify(formData));
                if (typeof (formData.id) === 'undefined' || !formData.id.trim()) {
                    console.log('Store::trigger -> posting (no id)');
                    props.endpoint = props.endpoint + '/';
                    (new Post_1.Post()).trigger(props, state)
                        .then(function () {
                        console.log("post done");
                        resolve("posting done");
                    })["catch"]((function (reason) {
                        reject(reason);
                    }));
                }
                else {
                    console.log('Store::trigger -> patching (' + formData.id + ')');
                    props.endpoint = props.endpoint + '/' + formData.id;
                    (new Patch_1.Patch()).trigger(props, state)
                        .then(function () {
                        console.log('Patching done');
                        resolve('patching done');
                    });
                }
            });
        };
        return Store;
    }(AbstractEvent_1.AbstractEvent));
    exports.Store = Store;
});
//# sourceMappingURL=Store.js.map