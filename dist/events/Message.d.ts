import { AbstractEvent } from "../AbstractEvent";
import { BasePromiseType } from "./base/BasePromiseType";
import { IEvent } from "../IEvent";
import { MessageState } from "../types/Message/MessageState";
import { MessageProps } from "../types/Message/MessageProps";
export declare class Message<P extends MessageProps, S extends MessageState, PT extends BasePromiseType> extends AbstractEvent<P, S, PT> implements IEvent<P, S, PT> {
    promiseType: PT;
    propType: P;
    stateType: S;
    trigger(props: MessageProps, state: MessageState): Promise<PT>;
}
