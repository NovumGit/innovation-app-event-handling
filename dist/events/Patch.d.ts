import { AbstractEvent } from "../AbstractEvent";
import { IEvent } from "../IEvent";
import { BasePromiseType } from "./base/BasePromiseType";
import { PatchProps } from "../types/Patch/PatchProps";
import { PatchState } from "../types/Patch/PatchState";
export declare class Patch<P extends PatchProps, S extends PatchState, PT extends BasePromiseType> extends AbstractEvent<P, S, PT> implements IEvent<P, S, PT> {
    promiseType: PT;
    propType: P;
    stateType: S;
    trigger(props: PatchProps, state: PatchState): Promise<PT>;
}
