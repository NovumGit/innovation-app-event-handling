import * as t from "../types/overheid";
import { AbstractEvent } from "../AbstractEvent";
import { BaseState } from "./base/BaseState";
import { BaseProps } from "./base/BaseProps";
import { BasePromiseType } from "./base/BasePromiseType";
import { IEvent } from "../IEvent";
export interface StoreState extends BaseState {
    context: {
        form: {
            id: string;
        };
    };
}
export interface StoreProps extends BaseProps {
    datasource: t.datasource;
    defaults: {};
    endpoint: string;
}
export declare class Store<P extends StoreProps, S extends StoreState, PT extends BasePromiseType> extends AbstractEvent<P, S, PT> implements IEvent<P, S, PT> {
    promiseType: PT;
    propType: P;
    stateType: S;
    trigger(props: P, state: S): Promise<PT>;
}
