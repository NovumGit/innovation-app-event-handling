var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "../AbstractEvent", "../types/StatusMessage/StatusMessage"], function (require, exports, AbstractEvent_1, StatusMessage_1) {
    "use strict";
    exports.__esModule = true;
    var StatusModal = (function (_super) {
        __extends(StatusModal, _super);
        function StatusModal() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        StatusModal.prototype.trigger = function (props, state) {
            var _this = this;
            return new Promise(function (resolve) {
                if (state instanceof StatusMessage_1.StatusValidationState) {
                    if (state.errors) {
                        state.message = '';
                        state.errors.forEach(function (e) {
                            state.message += "- " + e + "<br>\n";
                        });
                    }
                }
                if (state.message) {
                    _this.show(state.StatusMessageColor, state.message, state.title, state.elementId);
                    resolve();
                }
            });
        };
        StatusModal.prototype.getTemplate = function (sType, sLabel, sMessage, sElementId, aButtons) {
            var sTemplate = '';
            if ($('#' + sElementId).length <= 0) {
                sTemplate += '       <div class="auto-modal popup-basic bg-none mfp-with-anim">';
                sTemplate += '          <div class="panel">';
                sTemplate += '              <div class="panel-heading">';
                sTemplate += '                  <span class="panel-icon">';
                sTemplate += '                      <i class="fa fa-' + sType + ' text-' + sType + '" />';
                sTemplate += '                  </span>';
                sTemplate += '                  <span class="panel-title text-' + sType + '">';
                sTemplate += '                      ' + sLabel;
                sTemplate += '                  </span>';
                sTemplate += '              </div>';
                sTemplate += '              <div class="panel-body">';
                sTemplate += '                  <p>';
                sTemplate += '                      ' + sMessage;
                sTemplate += '                  </p>';
                sTemplate += '              </div>';
                sTemplate += '              <div class="panel-footer text-right">';
                if (aButtons && aButtons.length > 0) {
                    aButtons.forEach(function (e) {
                        sTemplate += '       <a title="' + e.title + '" class="btn btn-default btn-' + e.type + '" href="' + e.action + '">';
                        sTemplate += '          ' + e.label;
                        sTemplate += '       </a>';
                    });
                }
                else {
                    sTemplate += '       <button type="button" class="btn btn-default" id="btn_modal_close">Sluiten</button>';
                }
                sTemplate += '       </div>';
                sTemplate += '       </div>';
                sTemplate += '       </div>';
            }
            else {
                console.log("Already showing message with id " + sElementId + ", skipping for now.");
            }
            return sTemplate;
        };
        StatusModal.prototype.show = function (sType, sMessage, sTitle, sElementId) {
            var sTemplate = this.getTemplate(sType, sTitle, sMessage, sElementId);
            if (sTemplate) {
                $('#status_modal_container').html(sTemplate);
                $.magnificPopup.open({
                    removalDelay: 500,
                    items: {
                        src: '.auto-modal'
                    },
                    callbacks: {
                        beforeOpen: function () {
                            this.st.mainClass = 'mfp-zoomIn';
                        }
                    },
                    midClick: true
                });
            }
        };
        return StatusModal;
    }(AbstractEvent_1.AbstractEvent));
    exports.StatusModal = StatusModal;
});
//# sourceMappingURL=StatusModal.js.map