define(["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var EventHandler = (function () {
        function EventHandler() {
        }
        EventHandler.trigger = function (event, props, state) {
            return new Promise(function (resolve, reject) {
                console.log('EventHandler.trigger');
                event.trigger(props, state).then(resolve())["catch"](reject());
            });
        };
        return EventHandler;
    }());
    exports.EventHandler = EventHandler;
});
//# sourceMappingURL=EventHandler.js.map