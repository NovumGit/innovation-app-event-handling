import { IEvent } from "./IEvent";
import { AbstractEvent } from "./AbstractEvent";
import { BaseProps } from "./events/base/BaseProps";
import { BaseState } from "./events/base/BaseState";
import { BasePromiseType } from "./events/base/BasePromiseType";
export declare class EventHandler<E extends IEvent<BaseProps, BaseState, BasePromiseType>, T extends AbstractEvent<BaseProps, BaseState, BasePromiseType>> {
    static trigger<T extends AbstractEvent<BaseProps, BaseState, BasePromiseType>>(event: T, props: T['propType'], state: T['stateType']): Promise<T['promiseType']>;
}
