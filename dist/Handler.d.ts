import { Event } from "./types/Event";
export declare class Handler {
    handle(eventSequence: Event[]): Promise<void>;
}
