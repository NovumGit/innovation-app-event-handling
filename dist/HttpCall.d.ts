import { HttpMethod } from "./types/Http/HttpMethod";
import { AjaxDone } from "./types/Http/AjaxDone";
import { AjaxFailHandler } from "./types/Http/AjaxFailHandler";
export declare function HttpCall(sUrl: string, eMethod: HttpMethod, data: string, done: AjaxDone, fail: AjaxFailHandler): void;
