define(["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    function HttpCall(sUrl, eMethod, data, done, fail) {
        var deleteRequest = $.ajax({
            url: sUrl,
            method: eMethod,
            contentType: "application/json",
            data: data,
            dataType: "json"
        });
        deleteRequest.done(function (msg) {
            console.log('resolve http call');
            console.log(msg.errors);
            if (msg.errors) {
                fail(msg);
                return;
            }
            done(msg);
        });
        deleteRequest.fail(function (jqXHR, textStatus) {
            console.log('fail http call');
            console.log(jqXHR);
            fail(textStatus);
        });
    }
    exports.HttpCall = HttpCall;
});
//# sourceMappingURL=HttpCall.js.map