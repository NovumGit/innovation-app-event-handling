define(["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var Util = (function () {
        function Util() {
        }
        Util.parseForm = function (formId) {
            console.log("parse form " + formId);
            var oForm = $('#' + formId).find('input, select');
            var dataArray = oForm.serializeArray();
            var formData = {};
            dataArray.forEach(function (e) {
                var name = e.name.replace('data[', '').replace(']', '');
                formData[name] = e.value;
            });
            console.log(JSON.stringify(formData));
            return formData;
        };
        return Util;
    }());
    exports.Util = Util;
});
//# sourceMappingURL=Util.js.map