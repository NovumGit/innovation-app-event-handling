define(["require", "exports", "./Handler", "./EventHandler", "./events/Delete", "./events/Message", "./events/Patch", "./events/Post", "./events/Redirect", "./events/Reload", "./events/StatusMessage", "./events/StatusModal", "./events/Store", "./events/base/BaseState", "./events/base/BaseProps", "./AbstractEvent", "./EventStack"], function (require, exports, Handler_1, EventHandler_1, Delete_1, Message_1, Patch_1, Post_1, Redirect_1, Reload_1, StatusMessage_1, StatusModal_1, Store_1, BaseState_1, BaseProps_1, AbstractEvent_1, EventStack_1) {
    "use strict";
    exports.__esModule = true;
    exports.Handler = Handler_1.Handler;
    exports.EventHandler = EventHandler_1.EventHandler;
    exports.Delete = Delete_1.Delete;
    exports.Message = Message_1.Message;
    exports.Patch = Patch_1.Patch;
    exports.Post = Post_1.Post;
    exports.Redirect = Redirect_1.Redirect;
    exports.Reload = Reload_1.Reload;
    exports.StatusMessage = StatusMessage_1.StatusMessage;
    exports.StatusModal = StatusModal_1.StatusModal;
    exports.Store = Store_1.Store;
    exports.BaseState = BaseState_1.BaseState;
    exports.BaseProps = BaseProps_1.BaseProps;
    exports.AbstractEvent = AbstractEvent_1.AbstractEvent;
    exports.EventStack = EventStack_1.EventStack;
    function index() {
        var eventButtons = document.getElementsByClassName('event_button');
        var _loop_1 = function (i) {
            eventButtons[i].addEventListener('click', function (event) {
                var sEventData = eventButtons[i].getAttribute('data-event');
                var aEventData = JSON.parse(sEventData);
                console.info("Received event data");
                var handler = new Handler_1.Handler();
                handler.handle(aEventData);
                event.preventDefault();
            });
        };
        for (var i = 0; i < eventButtons.length; i++) {
            _loop_1(i);
        }
    }
    exports.index = index;
});
//# sourceMappingURL=index.js.map