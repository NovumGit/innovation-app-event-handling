declare type formDataType = {
    [key: string]: string;
};
export declare class Util {
    static parseForm(formId: string): formDataType;
}
export {};
