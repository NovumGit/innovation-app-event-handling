import { IEvent } from "./IEvent";
import { BaseProps } from "./events/base/BaseProps";
import { BaseState } from "./events/base/BaseState";
import { BasePromiseType } from "./events/base/BasePromiseType";
import { AjaxFailHandler } from "./types/Http/AjaxFailHandler";
export declare abstract class AbstractEvent<P extends BaseProps, S extends BaseState, PT extends BasePromiseType> implements IEvent<P, S, PT> {
    abstract propType: P;
    abstract stateType: S;
    abstract promiseType: PT;
    abstract trigger(props: P, state: S): Promise<PT>;
    getPropType(): P;
    getStateType(): S;
    getRejectHandler(reject: any, title: string, message: string): AjaxFailHandler;
}
