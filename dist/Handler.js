var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
define(["require", "exports", "./events/Delete", "./events/Patch", "./events/Post", "./events/Redirect", "./events/Reload", "./events/StatusModal", "./events/StatusMessage", "./events/Store"], function (require, exports, Delete_1, Patch_1, Post_1, Redirect_1, Reload_1, StatusModal_1, StatusMessage_1, Store_1) {
    "use strict";
    exports.__esModule = true;
    var Handler = (function () {
        function Handler() {
        }
        Handler.prototype.handle = function (eventSequence) {
            return __awaiter(this, void 0, void 0, function () {
                var _i, eventSequence_1, event_1, err_1;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            console.log("Handler::handle");
                            _i = 0, eventSequence_1 = eventSequence;
                            _a.label = 1;
                        case 1:
                            if (!(_i < eventSequence_1.length)) return [3, 23];
                            event_1 = eventSequence_1[_i];
                            console.log("Handler::handle -> " + event_1.name);
                            _a.label = 2;
                        case 2:
                            _a.trys.push([2, 20, 21, 22]);
                            if (!(event_1.name === 'Delete')) return [3, 4];
                            return [4, (new Delete_1.Delete).trigger(event_1.props, event_1.state)];
                        case 3:
                            _a.sent();
                            return [3, 19];
                        case 4:
                            if (!(event_1.name === 'Patch')) return [3, 6];
                            return [4, (new Patch_1.Patch).trigger(event_1.props, event_1.state)];
                        case 5:
                            _a.sent();
                            return [3, 19];
                        case 6:
                            if (!(event_1.name === 'Post')) return [3, 8];
                            return [4, (new Post_1.Post).trigger(event_1.props, event_1.state)];
                        case 7:
                            _a.sent();
                            return [3, 19];
                        case 8:
                            if (!(event_1.name === 'Redirect')) return [3, 10];
                            return [4, (new Redirect_1.Redirect).trigger(event_1.props)];
                        case 9:
                            _a.sent();
                            return [3, 19];
                        case 10:
                            if (!(event_1.name === 'Reload')) return [3, 12];
                            return [4, (new Reload_1.Reload).trigger()];
                        case 11:
                            _a.sent();
                            return [3, 19];
                        case 12:
                            if (!(event_1.name === 'StatusModal')) return [3, 14];
                            return [4, (new StatusModal_1.StatusModal).trigger(event_1.props, event_1.state)];
                        case 13:
                            _a.sent();
                            return [3, 19];
                        case 14:
                            if (!(event_1.name === 'StatusMessage')) return [3, 16];
                            return [4, (new StatusMessage_1.StatusMessage).trigger(event_1.props, event_1.state)];
                        case 15:
                            _a.sent();
                            return [3, 19];
                        case 16:
                            if (!(event_1.name === 'Store')) return [3, 18];
                            return [4, (new Store_1.Store).trigger(event_1.props, event_1.state)];
                        case 17:
                            _a.sent();
                            return [3, 19];
                        case 18:
                            console.error("event missing " + event_1.name);
                            _a.label = 19;
                        case 19: return [3, 22];
                        case 20:
                            err_1 = _a.sent();
                            console.error(err_1);
                            return [3, 22];
                        case 21:
                            console.log("Handler::done!");
                            return [7];
                        case 22:
                            _i++;
                            return [3, 1];
                        case 23: return [2];
                    }
                });
            });
        };
        return Handler;
    }());
    exports.Handler = Handler;
});
//# sourceMappingURL=Handler.js.map