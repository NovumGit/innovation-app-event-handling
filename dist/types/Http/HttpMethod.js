define(["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var HttpMethod;
    (function (HttpMethod) {
        HttpMethod["GET"] = "GET";
        HttpMethod["POST"] = "POST";
        HttpMethod["PUT"] = "PUT";
        HttpMethod["DELETE"] = "DELETE";
        HttpMethod["PATCH"] = "PATCH";
    })(HttpMethod = exports.HttpMethod || (exports.HttpMethod = {}));
});
//# sourceMappingURL=HttpMethod.js.map