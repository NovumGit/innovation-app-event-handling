export declare type AjaxDone = (message: string) => Promise<string>;
