import { BaseState } from "../../events/base/BaseState";
export declare class MessageState implements BaseState {
    context: {
        form: {
            id: string;
        };
    };
}
