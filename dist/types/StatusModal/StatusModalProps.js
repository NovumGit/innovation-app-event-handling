var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "../../events/base/BaseProps"], function (require, exports, BaseProps_1) {
    "use strict";
    exports.__esModule = true;
    var StatusModalProps = (function (_super) {
        __extends(StatusModalProps, _super);
        function StatusModalProps() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return StatusModalProps;
    }(BaseProps_1.BaseProps));
    exports.StatusModalProps = StatusModalProps;
});
//# sourceMappingURL=StatusModalProps.js.map