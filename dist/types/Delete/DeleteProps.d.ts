import { BaseProps } from "../../events/base/BaseProps";
import * as t from "../overheid";
export declare class DeleteProps implements BaseProps {
    datasource: t.datasource;
    endpoint: string;
}
