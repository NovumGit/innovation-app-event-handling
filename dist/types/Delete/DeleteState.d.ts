import { BaseState } from "../../events/base/BaseState";
export declare class DeleteState implements BaseState {
    id: string;
    context: {
        form: {
            id: string;
        };
    };
}
