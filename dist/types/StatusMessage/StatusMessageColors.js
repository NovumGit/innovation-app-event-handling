define(["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var StatusMessageColors;
    (function (StatusMessageColors) {
        StatusMessageColors["info"] = "info";
        StatusMessageColors["success"] = "success";
        StatusMessageColors["alert"] = "alert";
        StatusMessageColors["warning"] = "warning";
        StatusMessageColors["danger"] = "danger";
    })(StatusMessageColors = exports.StatusMessageColors || (exports.StatusMessageColors = {}));
});
//# sourceMappingURL=StatusMessageColors.js.map