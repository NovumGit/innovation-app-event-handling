import { IEvent } from "../../IEvent";
import { BaseProps } from "../../events/base/BaseProps";
import { BaseState } from "../../events/base/BaseState";
import { BasePromiseType } from "../../events/base/BasePromiseType";
import { EventStack } from "../../EventStack";
import { AbstractEvent } from "../../AbstractEvent";
import { StatusMessageColors } from "./StatusMessageColors";
export interface StatusMessageButton {
    label: string;
    action: IEvent<BaseProps, BaseState, BasePromiseType> | EventStack<IEvent<BaseProps, BaseState, BasePromiseType>, AbstractEvent<BaseProps, BaseState, BasePromiseType>>;
    title: string;
    type: StatusMessageColors;
}
