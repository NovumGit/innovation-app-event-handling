export declare enum StatusMessageColors {
    info = "info",
    success = "success",
    alert = "alert",
    warning = "warning",
    danger = "danger"
}
