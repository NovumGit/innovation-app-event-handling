import { BaseState } from "../events/base/BaseState";
import { StatusMessageColors } from "./StatusMessage/StatusMessageColors";
import { StatusMessageButton } from "./StatusMessage/StatusMessageButton";
export declare class StatusState implements BaseState {
    message?: string;
    title: string;
    elementId: string;
    StatusMessageColor: StatusMessageColors;
    buttons?: StatusMessageButton[];
}
