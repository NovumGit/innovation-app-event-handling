import { BaseState } from "../../events/base/BaseState";
export declare class PatchState implements BaseState {
    context: {
        form: {
            id: string;
        };
    };
}
