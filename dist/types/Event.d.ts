export declare type Event = {
    name: string;
    props: any;
    state: any;
};
