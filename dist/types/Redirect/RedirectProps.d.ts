import { BaseProps } from "../../events/base/BaseProps";
export declare class RedirectProps implements BaseProps {
    location: string;
    endpoint: string;
}
