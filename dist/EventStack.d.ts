import { IEvent } from "./IEvent";
import { AbstractEvent } from "./AbstractEvent";
import { BaseProps } from "./events/base/BaseProps";
import { BaseState } from "./events/base/BaseState";
import { BasePromiseType } from "./events/base/BasePromiseType";
export declare class EventStack<E extends IEvent<BaseProps, BaseState, BasePromiseType>, T extends AbstractEvent<BaseProps, BaseState, BasePromiseType>> {
    stack: {
        handler: T;
        props: T['propType'];
        state: T['stateType'];
    }[];
    register(event: T, props: T['propType'], state: T['stateType']): void;
    execute: () => Promise<BasePromiseType[]>;
}
