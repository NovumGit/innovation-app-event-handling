import {IEvent} from "./IEvent";
import {AbstractEvent} from "./AbstractEvent";
import {EventHandler} from "./EventHandler";
import {BaseProps} from "./events/base/BaseProps";
import {BaseState} from "./events/base/BaseState";
import {BasePromiseType} from "./events/base/BasePromiseType";


export class EventStack<E extends IEvent<BaseProps, BaseState, BasePromiseType>,
    T extends AbstractEvent<BaseProps, BaseState, BasePromiseType>> {

    stack: { handler: T, props: T['propType'], state: T['stateType'] }[];

    register(event: T, props: T['propType'], state: T['stateType']) {
        this.stack.push({handler: event, props, state})
    }

    execute = (): Promise<BasePromiseType[]> => {
        return new Promise<BasePromiseType[]>(async (resolve, reject) => {

            const results: BasePromiseType[] = [];
            const errors: string[] = [];
            for(const event of this.stack)
            {
                try {
                    const result = await EventHandler.trigger(event.handler, event.props, event.state);
                    results.push(result);
                } catch (e) {
                    errors.push(e);
                }
            }

            if (errors.length) {
                reject(errors);
                return;
            }
            resolve(results);
        });


    }

}
