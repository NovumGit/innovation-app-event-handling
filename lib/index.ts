import {Handler} from "./Handler";
import {EventHandler} from "./EventHandler";
import {Delete} from "./events/Delete";
import {Message} from "./events/Message";
import {Patch} from "./events/Patch";
import {Post} from "./events/Post";
import {Redirect} from "./events/Redirect";
import {Reload} from "./events/Reload";
import {StatusMessage} from "./events/StatusMessage";
import {StatusModal} from "./events/StatusModal";
import {Store} from "./events/Store";
import {BaseState} from "./events/base/BaseState";
import {BaseProps} from "./events/base/BaseProps";
import {BasePromiseType} from "./events/base/BasePromiseType";
import {AbstractEvent} from "./AbstractEvent";
import {IEvent} from "./IEvent";
import {EventStack} from "./EventStack";

export {
    AbstractEvent,
    EventHandler,
    Handler,
    Delete,
    Message,
    Patch,
    Post,
    Redirect,
    Reload,
    StatusMessage,
    StatusModal,
    Store,
    BaseState,
    BaseProps,
    BasePromiseType,
    IEvent,
    EventStack
};

export function index() {
    const eventButtons = document.getElementsByClassName('event_button');

    /* tslint:disable */
    for(let i = 0; i < eventButtons.length; i++)
    {
        eventButtons[i].addEventListener('click', (event) => {

            const sEventData = eventButtons[i].getAttribute('data-event');
            const aEventData = JSON.parse(sEventData);

            console.info("Received event data");
            const handler = new Handler();
            handler.handle(aEventData);
            event.preventDefault();
        });
    }
}
