import {IEvent} from "./IEvent";
import {AbstractEvent} from "./AbstractEvent";
import {BaseProps} from "./events/base/BaseProps";
import {BaseState} from "./events/base/BaseState";
import {BasePromiseType} from "./events/base/BasePromiseType";

export class EventHandler<E extends IEvent<BaseProps, BaseState, BasePromiseType>, T extends AbstractEvent<BaseProps, BaseState, BasePromiseType>>{


    static trigger<T extends AbstractEvent<BaseProps, BaseState, BasePromiseType>>(event : T, props : T['propType'], state :  T['stateType']):Promise<T['promiseType']>
    {
        return new Promise<BasePromiseType>((resolve : any, reject : any) => {

            console.log('EventHandler.trigger');
            event.trigger(props, state).then(resolve()).catch(reject());

        })



    }
}
