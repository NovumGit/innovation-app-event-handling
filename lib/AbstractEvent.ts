import {IEvent} from "./IEvent";
import {StatusValidationState} from "./types/StatusMessage/StatusMessage";
import {BaseProps} from "./events/base/BaseProps";
import {BaseState} from "./events/base/BaseState";
import {BasePromiseType} from "./events/base/BasePromiseType";
import {StatusState} from "./types/StatusState";
import {StatusMessageColors} from "./types/StatusMessage/StatusMessageColors";
import {AjaxFailHandler} from "./types/Http/AjaxFailHandler";


export abstract class AbstractEvent<P extends BaseProps, S extends BaseState, PT extends BasePromiseType> implements IEvent<P, S, PT>{

    abstract propType : P;
    abstract stateType: S;
    abstract promiseType : PT;



    abstract trigger(props : P, state : S): Promise<PT>;

    getPropType() : P
    {
        return this.propType;
    }
    getStateType() : S
    {
        return this.stateType;
    }
    getRejectHandler(reject : any, title : string, message : string):AjaxFailHandler
    {
        return async (textStatus: any) => {


            console.log(textStatus);
            let errors = null;

            if(textStatus.errors)
            {
                errors = textStatus.errors;
            }

            if(errors)
            {
                // Validatie errors
                const oState:StatusValidationState = {
                    title,
                    errors,
                    elementId : "error_dialog",
                    StatusMessageColor : StatusMessageColors.warning
                };


                reject("Validation errors occured");
            }
            else
            {
                const oState:StatusState = {
                    title,
                    message : message.replace('{textStatus}', textStatus),
                    elementId : "error_dialog",
                    StatusMessageColor : StatusMessageColors.warning
                };
            }

        };
    }
}
