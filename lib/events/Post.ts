import {Util} from "../Util";
import * as t from "../types/overheid";
import {AbstractEvent} from "../AbstractEvent";
import {IEvent} from "../IEvent";
import {BaseProps} from "./base/BaseProps";
import {BaseState} from "./base/BaseState";
import {BasePromiseType} from "./base/BasePromiseType";
import {HttpMethod} from "../types/Http/HttpMethod";
import {HttpCall} from "../HttpCall";

export interface PostProps extends BaseProps{
    datasource : t.datasource;
    defaults : {};
    endpoint : string;
}

export interface PostState extends BaseState{
    context : {
        form : {
             id : string
        }
    }
}

export class Post<P extends PostProps, S extends PostState, PT extends BasePromiseType> extends AbstractEvent<BaseProps, BaseState, BasePromiseType>
    implements IEvent<BaseProps, BaseState, BasePromiseType>{
    propType: BaseProps;
    stateType: BaseState;
    promiseType: BasePromiseType;

    trigger(props : P, state : S):Promise<PT>
    {
        return new Promise((resolve : any, reject : any) => {

            const sUrl = props.datasource.url + props.endpoint;
            let data = Util.parseForm(state.context.form.id);

            if (props.defaults) {
                data = Object.assign(props.defaults, data);
            }

            const rejectHandler = this.getRejectHandler(reject, "Whoops", "Het aanmaken is mislukt");
            HttpCall(sUrl, HttpMethod.POST, JSON.stringify(data), resolve, rejectHandler);
        });

    }
}
