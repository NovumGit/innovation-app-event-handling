import {Util} from "../Util";
import {Post, PostProps, PostState} from "./Post";
import {Patch} from "./Patch";
import * as t from "../types/overheid";
import {AbstractEvent} from "../AbstractEvent";
import {BaseState} from "./base/BaseState";
import {BaseProps} from "./base/BaseProps";
import {BasePromiseType} from "./base/BasePromiseType";
import {IEvent} from "../IEvent";

export interface StoreState extends BaseState{
    context : {
        form : {
            id : string
        }
    }
}
export interface StoreProps extends BaseProps{
    datasource : t.datasource,
    defaults : {}
    endpoint : string
}


export class Store<
    P extends StoreProps,
    S extends StoreState,
    PT extends BasePromiseType> extends AbstractEvent<P, S, PT> implements IEvent<P, S, PT>{

    promiseType: PT;
    propType: P;
    stateType: S;

    trigger(props : P, state : S):Promise<PT>
    {
        return new Promise<PT>((resolve : any, reject : any) => {

            const formData = Util.parseForm(state.context.form.id);

            console.log('IForm data');
            console.log(JSON.stringify(formData));
            if (typeof(formData.id) === 'undefined' || !formData.id.trim()) {
                console.log('Store::trigger -> posting (no id)');
                props.endpoint = props.endpoint + '/';

                (new Post<PostProps, PostState, BasePromiseType>()).trigger(props, state)
                    .then(() => {
                        console.log("post done");
                        resolve("posting done");
                    }).catch((reason => {
                        reject(reason);
                    })
                );

            } else {
                console.log('Store::trigger -> patching (' + formData.id + ')');
                props.endpoint = props.endpoint + '/' + formData.id;

                (new Patch()).trigger(props, state)
                    .then(() => {
                        console.log('Patching done');
                        resolve('patching done');
                    });
            }

        });

    }
}

