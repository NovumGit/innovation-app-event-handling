import {IEvent} from "../IEvent";
import {AbstractEvent} from "../AbstractEvent";
import {BaseProps} from "./base/BaseProps";
import {BaseState} from "./base/BaseState";
import {BasePromiseType} from "./base/BasePromiseType";

export class Reload<P extends BaseProps, S extends BaseState, PT extends BasePromiseType>
extends AbstractEvent<P, S, PT>
implements IEvent<P, S, PT>{

    promiseType: PT;
    propType: P;
    stateType: S;

    trigger():Promise<PT>
    {
        return new Promise<PT>(() => {

            window.location = window.location;
        });

    }
}
