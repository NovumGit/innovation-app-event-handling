import {IEvent} from "../IEvent";
import {StatusValidationState} from "../types/StatusMessage/StatusMessage";
import {AbstractEvent} from "../AbstractEvent";
import {BaseProps} from "./base/BaseProps";
import {StatusState} from "../types/StatusState";
import {BasePromiseType} from "./base/BasePromiseType";
import {StatusMessageColors} from "../types/StatusMessage/StatusMessageColors";

export interface StatusMessageProps extends BaseProps {
}

export class StatusMessage<P extends StatusMessageProps, S extends StatusState, PT extends BasePromiseType>
    extends AbstractEvent<P, S, PT> implements IEvent<P, S, PT> {

    promiseType: PT;
    propType: P;
    stateType: S;
    getPropType() : P {
        return this.propType;
    }
    getStateType() : S {
        return this.stateType;
    }

    trigger(props: P, state: StatusState): Promise<PT> {
        return new Promise((resolve: any) => {
            if (state instanceof StatusValidationState) {
                state.errors.forEach((e) => {
                    state.message = "- " + e + "\n";
                });
            }

            if (state.message) {
                StatusMessage.show(state.StatusMessageColor, state.message, state.title, state.elementId);
            }
            resolve("Showing status message");
        });
    }

    private static getTemplate(sType: StatusMessageColors, sLabel: string, sMessage: string, sElementId: string) {

        const sLabelColor = sType === 'warning' ? 'black' : 'white';
        let sTemplate = '';
        // sTemplate += '<section id="content" class="table-layout animated fadeIn">';
        // sTemplate += '   <div class="panel">';

        if ($('#' + sElementId).length <= 0) {
            // if($('#' + sElementId).length )
            sTemplate += '       <div class="bs-component" id="' + sElementId + '">';
            sTemplate += '           <div class="alert alert-' + sType + ' dark alert-dismissable" style="color:' + sLabelColor + ';">';
            sTemplate += '               <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            sTemplate += '               <i class="fa fa-info pr10" />';
            sTemplate += '               <strong>' + sLabel + ':</strong> ' + sMessage;
            sTemplate += '           </div>';
            sTemplate += '       </div>';
            // sTemplate += '   </div>';
            // sTemplate += '</section>';
        } else {
            console.log("Already showing message with id " + sElementId + ", skipping for now.");
        }
        return sTemplate;
    }

    private static show(sType: StatusMessageColors, sMessage: string, sTitle: string, sElementId: string) {
        const sTemplate = StatusMessage.getTemplate(sType, sTitle, sMessage, sElementId);
        if (sTemplate) {
            $('#status_message_container').html(sTemplate);
        }
    }

    /*
    private static success(sTitle: string, sMessage: string, sElementId: string) {
        this.show(sT.StatusMessageColors.success, sMessage, sTitle, sElementId);
    }
    private static alert(sTitle: string, sMessage: string, sElementId: string) {
        this.show(sT.StatusMessageColors.alert, sMessage, sTitle, sElementId);
    }
    private static warning(sTitle: string, sMessage: string, sElementId: string) {
        this.show(sT.StatusMessageColors.warning, sMessage, sTitle, sElementId);
    }
    private static danger(sTitle: string, sMessage: string, sElementId: string) {
        this.show(sT.StatusMessageColors.danger, sMessage, sTitle, sElementId);
    }
    */
}
