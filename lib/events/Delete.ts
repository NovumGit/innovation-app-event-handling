import {AbstractEvent} from "../AbstractEvent";
import {IEvent} from "../IEvent";
import {BasePromiseType} from "./base/BasePromiseType";
import {HttpCall} from "../HttpCall";
import {HttpMethod} from "../types/Http/HttpMethod";
import {DeleteProps} from "../types/Delete/DeleteProps";
import {DeleteState} from "../types/Delete/DeleteState";

export class Delete <P extends DeleteProps, S extends DeleteState, PT extends BasePromiseType> extends AbstractEvent<DeleteProps, DeleteState, BasePromiseType> implements IEvent<DeleteProps, DeleteState, BasePromiseType>{

    stateType : S;
    propType: P;
    promiseType: PT;

    trigger(props : P, state: S):Promise<PT>
    {
        return new Promise((resolve:any, reject:any) => {

            const sUrl = props.datasource.url + props.endpoint;
            const rejectHandler = this.getRejectHandler(reject, "Whoops", "Sorry we where unable to delete that item\n {textStatus}");

            HttpCall(sUrl, HttpMethod.DELETE, '', resolve, rejectHandler);
        });
    }


}
