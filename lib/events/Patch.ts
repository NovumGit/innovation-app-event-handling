import {Util} from "../Util";
import {AbstractEvent} from "../AbstractEvent";
import {IEvent} from "../IEvent";
import {BasePromiseType} from "./base/BasePromiseType";
import {HttpMethod} from "../types/Http/HttpMethod";
import {HttpCall} from "../HttpCall";
import {PatchProps} from "../types/Patch/PatchProps";
import {PatchState} from "../types/Patch/PatchState";

export class Patch<P extends PatchProps, S extends PatchState, PT extends BasePromiseType>
    extends AbstractEvent<P, S, PT>
    implements IEvent<P, S, PT>{

    promiseType : PT;
    propType : P;
    stateType : S;

    trigger(props : PatchProps, state : PatchState):Promise<PT>
    {
        return new Promise((resolve : any, reject : any) => {

            const sUrl = props.datasource.url + props.endpoint;
            let data = Util.parseForm(state.context.form.id);

            if (props.defaults) {
                data = Object.assign(props.defaults, data);
            }

            const rejectHandler = this.getRejectHandler(reject, "Whoops", "Het wijzigen is mislukt");

            HttpCall(sUrl, HttpMethod.POST, JSON.stringify(data), resolve, rejectHandler);
        });

    }
}
