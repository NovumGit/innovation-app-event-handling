import {Util} from "../Util";
import {AbstractEvent} from "../AbstractEvent";
import {BasePromiseType} from "./base/BasePromiseType";
import {IEvent} from "../IEvent";
import {HttpMethod} from "../types/Http/HttpMethod";
import {HttpCall} from "../HttpCall";
import {MessageState} from "../types/Message/MessageState";
import {MessageProps} from "../types/Message/MessageProps";


export class Message<P extends MessageProps, S extends MessageState, PT extends BasePromiseType>
    extends AbstractEvent<P, S, PT>
    implements IEvent<P, S, PT>{

    promiseType: PT;
    propType: P;
    stateType: S;

    trigger(props : MessageProps, state : MessageState):Promise<PT>
    {
        return new Promise((resolve : any, reject : any) => {

            const sUrl = props.datasource.url + props.endpoint;
            let data = Util.parseForm(state.context.form.id);

            if (props.defaults) {
                data = Object.assign(props.defaults, data);
            }

            const rejectHandler = this.getRejectHandler(reject, "Whoops", "Het wijzigen is mislukt");

            HttpCall(sUrl, HttpMethod.POST, JSON.stringify(data), resolve, rejectHandler);
        });

    }
}
