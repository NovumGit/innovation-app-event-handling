import {HttpMethod} from "./types/Http/HttpMethod";
import {AjaxDone} from "./types/Http/AjaxDone";
import {AjaxFailHandler} from "./types/Http/AjaxFailHandler";

export function HttpCall(sUrl : string, eMethod : HttpMethod, data : string, done : AjaxDone, fail : AjaxFailHandler) {

    const deleteRequest = $.ajax({
        url: sUrl,
        method: eMethod,
        contentType: "application/json",
        data,
        dataType: "json"
    });

    deleteRequest.done((msg) => {
        console.log('resolve http call');
        console.log(msg.errors);
        if(msg.errors)
        {
            fail(msg);
            return;
        }
        done(msg);
    });

    deleteRequest.fail((jqXHR, textStatus) => {
        console.log('fail http call');
        console.log(jqXHR);
        fail(textStatus);
    });
}
