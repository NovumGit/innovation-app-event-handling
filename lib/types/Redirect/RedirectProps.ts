import {BaseProps} from "../../events/base/BaseProps";

export class RedirectProps implements BaseProps{
    location : string;
    endpoint:string;
}
