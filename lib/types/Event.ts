export type Event = {
    name: string,
    props: any,
    state: any
};

