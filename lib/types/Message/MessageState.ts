import {BaseState} from "../../events/base/BaseState";

export class MessageState implements BaseState{
    context : {
        form : {
            id : string
        }
    }
}
