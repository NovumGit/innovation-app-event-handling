import {BaseProps} from "../../events/base/BaseProps";
import * as t from "../overheid";

export class MessageProps implements BaseProps{
    datasource : t.datasource;
    endpoint : string;
    defaults : {}
}
