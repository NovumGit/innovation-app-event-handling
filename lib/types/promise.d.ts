export interface pResolve {
    (message: string): string;
}

export interface pReject {
    (textStatus: string): string;
}
