export interface datasource {
    id : number,
    titel : string,
    code : string,
    url : string,
    documentation : string
}
