import {BaseProps} from "../../events/base/BaseProps";
import * as t from "../overheid";
export class PatchProps implements BaseProps{
    datasource : t.datasource;
    endpoint : string;
    defaults : {}
}
