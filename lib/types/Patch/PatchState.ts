import {BaseState} from "../../events/base/BaseState";

export class PatchState implements BaseState{
    context : {
        form : {
            id : string
        }
    }
}
