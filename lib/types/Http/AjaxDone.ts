export type AjaxDone = (message: string) => Promise<string>;
