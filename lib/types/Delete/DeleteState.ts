import {BaseState} from "../../events/base/BaseState";

export class DeleteState implements BaseState {
    id : string;
    context : {
        form : {
            id : string
        }
    }
}
