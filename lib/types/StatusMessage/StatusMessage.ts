import {ValidationErrors} from "../ValidationErrors";
import {StatusMessageColors} from "./StatusMessageColors";
import {StatusState} from "../StatusState";

export class StatusValidationState implements StatusState{
    errors: ValidationErrors;
    message?: string;
    title : string;
    elementId : string;
    StatusMessageColor : StatusMessageColors
}
