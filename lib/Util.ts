type formDataType = { [key: string]: string; };

export class Util {
    static parseForm (formId :string):formDataType{
        console.log("parse form " + formId);

        const oForm = $('#' + formId).find('input, select');

        const dataArray = oForm.serializeArray();
        const formData:formDataType = {};

        // console.log(dataArray);
        dataArray.forEach((e) => {

            const name = e.name.replace('data[', '').replace(']', '');
            // console.log('1', name, e.value);
            // console.log('e', e);
            formData[name] = e.value;
        });

        console.log(JSON.stringify(formData));
        return formData;
    }
}


