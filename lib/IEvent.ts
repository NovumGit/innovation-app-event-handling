import {BaseProps} from "./events/base/BaseProps";
import {BaseState} from "./events/base/BaseState";
import {BasePromiseType} from "./events/base/BasePromiseType";


export interface IEvent<propertyType extends BaseProps, stateType extends BaseState, promiseType extends BasePromiseType> {



    propType : propertyType;
    stateType : stateType;
    promiseType : promiseType;

    trigger(props : propertyType, state : stateType, ): Promise<promiseType>;

}
